# En måte å åpne en 'peker' til en fil som skal leses:
f = open('vold.txt','r',encoding="utf-8")
# encoding-greia kan du bruke hvis du ender opp
# med rare tegn som 'Ã¥' der det skal stå 'å'.

# f peker nå til det en kan lese fra.

# Nå kan en gjøre hva en vil med ting fra filen:
# Eksempel 1: lese inn én og én linje, og skrive
# ut innholdet samt antall tegn på linja.
fil = f.read()
dict = {}
#
for ord in fil.split():
    exists = dict.get(len(ord),[])
#    print(exists)
    dict[len(ord)] = exists + [ord]
#dict = {len(ord):dict.get(len(ord),[])+[ord] for ord in fil.split()}
for key, value in dict.items():
    print(key,value)
#    for val in value:
 #       print(val)

# En må stenge pekeren til filen:
f.close()