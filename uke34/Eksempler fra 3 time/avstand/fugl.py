# Luftlinje mellom to steder på jorda!
# Må importere noe som allerede har gjort jobben for oss!
from geopy.geocoders import Nominatim
from geopy import distance
from geopy.distance import geodesic 

geolocator = Nominatim(user_agent="foo") # Denne bare gjør noe, ikke tenk på det.

location1 = geolocator.geocode(input(f"Sted 1: "))
print("Sted 1:",location1)
location2 = geolocator.geocode(input(f"Sted 2: "))
print("Sted 2:",location2)

km = geodesic((location1.latitude, location1.longitude), (location2.latitude, location2.longitude)).km
print(f"Flakselengden mellom disse er {km:.2f} km.")
