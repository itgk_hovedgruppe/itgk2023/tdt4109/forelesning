def tell_vokaler(tekst):
    antall = 0
    vokaler = 'aeuioyæøå'
    for bokstav in tekst:
        if bokstav in vokaler:
            antall += 1;
    return antall

def tell_konsonanter(tekst):
    antall = 0
    konsonanter = 'bcdfghjklmnpqrstvwxz'
    for bokstav in tekst:
        print(bokstav)
        if bokstav in konsonanter:
            antall += 1
    return antall

streng = input('skriv inn teksten: ')
svar = tell_vokaler(streng)
print(svar)
print(tell_konsonanter(streng))