streng = input("Skriv inn tekst som jeg skal analysere: ")

antall = {}
for bokstav in streng:
    if bokstav not in antall:
        antall[bokstav] = 0
    antall[bokstav] = antall[bokstav] + 1

for bokstav in sorted(antall):
    print(f"{bokstav}: {antall[bokstav]}")