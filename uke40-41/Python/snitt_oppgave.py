liste = [1,2,3,54]

# Det er to ting dere trenger å vite om lister for å gjøre dette
# sum(liste) summerer alle elementer, hvis det er tall
# len(liste) returnerer hvor mange elementer det er i listen

# Oppgave 1
# Beregn og skriv ut gjennomsnittet av tall i en liste
print(f"Summen av {liste} er {sum(liste)/len(liste)}")


# Oppgave 2
# Flytt beregningen inn i en funksjon beregn_snitt(liste)
# som skriver ut snittet av listen den får inn
def beregn_snitt(liste):
    print(f"Summen av {liste} er {sum(liste)/len(liste)}")

beregn_snitt(liste)
# Oppgave 3
# Endre funksjonen så den i stedet returnerer snittverdien
# endre kallet til funksjonen slik at resultatet blir det samme
def beregn_snitt(liste):
    return sum(liste)/len(liste)

print((f"Summen av {liste} er {beregn_snitt(liste)}"))

# Oppgave 4
# Lag og kall en funksjon legg_til(liste):
# Spør brukeren om å skrive inn et heltall, og legg dette
# til i listen som et heltall, og returner listen.
# Dere trenger EN ny ting:
# liste.append(4) legger til 4 i slutten av liste.
def legg_til(liste):
    tall = input("Skriv inn et heltall: ")
    faktisk_tall = int(tall)
    liste.append(faktisk_tall)
    return liste

# liste = legg_til(liste)
# print(f"Snittet av listen {liste} er {beregn_snitt(liste)}")

# Oppgave 5 - minmax
# Lag funksjonen minmax(liste) og kall den med liste
# Den skal returnere et tuppel med minimums- og maksimumsverdien
# i listen: så det ser ut som dette: (1,54)
# Nye ting: min(liste), max(liste), et tuppel er slik: ()
def minmax(liste):
    return min(liste), max(liste)

print(minmax(liste))

# Oppgave 6: for_alle(liste)
# Lag og kall funksjonen for_alle.
# Den skal skrive ut følgende (eksempeltall) for hvert element:
# Verdi: 1
# tips: for verdi in liste:
def for_alle(liste):
    for i, elem in enumerate(liste):
        print(f"på plass nummer {i} ligger {elem}.")
        
for_alle(liste)

# oppgave 7: er_3_i_listen(liste)
# funksjonen returnerer om 3 er i listen
# "ost" in [1, 2, 4, "ost"] -> True
def er_3_i_listen(liste):
    for tall in liste:
        if tall == 3:
            return True
    return False


print(er_3_i_listen(liste))
