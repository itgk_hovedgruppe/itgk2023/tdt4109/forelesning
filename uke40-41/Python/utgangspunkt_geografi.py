# Kahoot: https://create.kahoot.it/share/starting-out-with-python-5-7-5-10-functions-with-return-value/c9dda4bc-b7d2-4685-bea3-2babd2249a42
'''

'''

land = ['Norge','Sverige','Spania','USA','Finland','Island',
    'Danmark','Tyskland','Frankrike']

hovedsteder = ['Oslo','Stockholm','Madrid','Washington, D.C.',
           'Helsinki','Reykjavik','København','Berlin','Paris']


# Samtaletema 1: Kom på minst to ulike måter å trekke et tilfeldig land.
# Samtaletema 1.1: Hvis du skal hente frem riktig by, hvordan vil dette
#                  innskrenke våre muligheter?

# La meg lage funksjonen gjett_land(land, hovedsteder)
import random

def gjett_land(land, hovedsteder):
    plass = random.randint(0,len(land))
    gjett = input(f"Hva er hovedstaden i {land[plass]}: ")
    if gjett == hovedsteder[plass]:
        print("Yess!")
    else:
        print("nopes")
        
# gjett_land(land, hovedsteder)

# Samtaletema 2: legg_til_land(land, hovedsteder)
# krav: land og hovedsteder skal kunne bli oppdatert uten for metoden ved hjelp av den
# Bli enige om minst to måter en kan løse dette på

def lag_nytt_land(land, hovedsteder):
    land.append(input("nytt land: "))
    hovedsteder.append(input("ny hovedstad: "))


# La meg kode legg_til_land, og kanskje vise muterbarhet med det samme.

# Samtaletema 3: Kan dere tenke på en annen måte å strukturere land og hovedsteder?
# Samtaletema 3.1: Hvordan vil strukturene se ut hvis man så for seg en liste med lister?
# Samtaletema 3.2: Kan dere komme på en annen måte å strukturere disse dataene?
#                  Trenger ikke tenke effektivitet.

# La meg kode om gjett_land

liste = [["Norge","Oslo"],["Sverige","Stockholm"]]
def gjett_land_2d(liste):
    valg = random.choice(liste)
    print("hovedstaden i",valg[0])
    gjettet = input()
    if gjettet == valg[1]:
        print("riktig")
    else:
        print("duh")

def lag_nytt_land_2d(liste):
    nytt_land = input("nytt land: ")
    ny_h = input("ny hovedstad: ")
    liste.append([nytt_land, ny_h])
    return liste

def print_liste_2d(liste):
    for l, h in liste:
        print(f"Hovedstaden i {l} er {h}.")


def en_d_til_2d(land, hovedsteder):
    liste = []
    for i in range(len(land)):
        liste.append([land[i],hovedsteder[i]])
    return liste

def to_d_til_1d(liste):
    land = []
    hovedsteder = []
    for lh in liste:
        land.append(lh[0])
        hovedsteder.append(lh[1])
    return land, hovedsteder
    


def grensesnitt(liste):    
    valg = True
    while valg != 'q':
        print("alternativ: g2, l2, p2, q")
        valg = (input("Valg: "))
        if (valg == "l2"):
            land = lag_nytt_land_2d(liste)        
        if (valg == "g2"):
            land = gjett_land_2d(liste)
        if (valg == "p2"):
            land = print_liste_2d(liste)
            
# grensesnitt(liste)
    
# Mulig videre ting: kryptering, grensesnitt

print(to_d_til_1d(liste))
