liste = [1,2,3,54]

# Det er to ting dere trenger å vite om lister for å gjøre dette
# sum(liste) summerer alle elementer, hvis det er tall
# len(liste) returnerer hvor mange elementer det er i listen

# Oppgave 1
# Beregn og skriv ut gjennomsnittet av tall i en liste
print(f"Snittet av listen {liste} er {sum(liste)/len(liste)}")


# Oppgave 2
# Flytt beregningen inn i en funksjon beregn_snitt(liste)
# som skriver ut snittet av listen den får inn

def beregn_snitt(liste):
    print(f"Snittet av listen {liste} er {sum(liste)/len(liste)}")
    
beregn_snitt(liste)

# Oppgave 3
# Endre funksjonen så den i stedet returnerer snittverdien
# endre kallet til funksjonen slik at resultatet blir det samme
def beregn_snitt(liste):
    return sum(liste)/len(liste)

print(f"Snittet av listen {liste} er {beregn_snitt(liste)}")

# Oppgave 4
# Lag en funksjon legg_til(liste):
# Sprø brukeren om å skrive inn et heltall, og legg dette
# til i listen som et heltall, og returner listen.
# Dere trenger EN ny ting:
# liste.append(4) legger til 4 i slutten av liste.

def legg_til(liste):
    ny = int(input("skriv inn et heltall: "))
    liste.append(ny)
    return liste

#liste = legg_til(liste)
#print(f"Snittet av listen {liste} er {beregn_snitt(liste)}")

# Oppgave 5 - minmax
# Lag funksjonen minmax(liste)
# Den skal returnere et tuppel med minimums- og maksimumsverdien
# i listen: så det ser ut som dette: (1,54)
# Nye ting: min(liste), max(liste), et tuppel er slik: ()
def minmax(liste):
    return (min(liste), max(liste))

print(minmax(liste))


# Oppgave 6: for_alle(liste)
# Lag og kall funksjonen for_alle.
# Den skal skrive ut følgende (eksempeltall) for hvert element:
# Verdi: 1
# Ny ting: for element in liste: (...)