#Denne uken:
# start med kahoot:

# Menti: 

# Så litt 'kjapt' om vs code og JN. Husk 'add to path'
# Nevne at en IKKE trenger å lage seg gitlabbruker, 4109 har en lokal.
# nå skal en uansett bare hente ned fra gitlab, ikke dytte noe opp.
# https://itgkhub.apps.stack.it.ntnu.no/hub/user-redirect/git-pull?repo=https%3A%2F%2Fgitlab.stud.idi.ntnu.no%2FITGKberegninger%2Fmateriale&urlpath=tree%2Fmateriale%2FJN0.ipynb
# Øverst høyre VS Code: Select kernel

# Vi kan yoinke noen andre JN med det samme:
# https://gitlab.stud.idi.ntnu.no/itgk_hovedgruppe/itgk2023/tdt4110/tdt4110-ovinger/-/tree/master/ForeleserNotebooks/uke36
# Den ene har har jeg lagt på min gitlab, for å vise ting.

# if-setninger og elif og else: kikke på calculateScores igjen.
# kolon og innrykk - hva definerer en blokk med kode
# Hva er egentlig en boolsk variabel: True og False
# relasjonsoperatorer < > >= <= == !=
# målanalyse - kan vi endre rekkefølge?
# puls: Dere skrive noe! 180 , 60
'''
Hvis puls over 180: 'Du trenger godt!'
hvis puls over 60: 'kom deg ut'
hvis puls 60 eller under: 'snork'
'''


# note to self - ififelse, ppt
# forstå frokost
# passord
# = vs == (tilordning vs sammenlikning)
# sammenlikne strenger (man vi rangere dem på noen måte?) (ord()/chr()) (kryptering?)
# sammenlikne variabel med tekststreng, er det mulig da? (hva er 'in'?)
# if inni if? what gives!?
# - utvide pulseksempel
# Se litt på mulige varianter i målsammenlikning - må en ha elif?
# hvis tid - skuddårseksempel fom slide omtrent 29
# def foo():
#     return (not True or False) or (not False and not (not 1 > 2)) #- logiske operatorer - vis debug!
# print(foo())
#1 <= x <= 100
# prioritet av betingelser: se prioritet_av_betingelser.py
# hvis tid: dørvaktsimulator (dorvaktsimulator.py)
