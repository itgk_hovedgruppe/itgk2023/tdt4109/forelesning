# Spør om navnet (eksempel Birgitte)
# Spør om alder (eksempel 17)

# Hvis personen er under 18 skriver du ut 'Du er for ung, Birgitte'
# Hvis personen er over 18 skriver du ut 'Velkommen, Birgitte'
# Hvis personen er under 21 skriver du også 'Men hold deg til øl.'

# Utvidelse hvis tid: Kan du på første linje i stedet skrive ut
# 'Kom tilbake om 1 år, Birgitte!'?
navn, alder = input("navn og alder:").split()
alder = int(alder)
if alder >= 18:
    print(f'Velkommen, {navn}')
    if alder < 21:
        print("Men hold deg til øl")
else:
    print(f"Du er for ung, {navn}")
    