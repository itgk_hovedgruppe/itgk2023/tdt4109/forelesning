# Eksempel på sammenligning av tekst

str1 = input('Skriv inn en streng: ')
str2 = input('Skriv inn en streng: ')
if str1 == str2:
    print(f'Begge strengene er {str1}.')
elif str1 < str2:
    print(f'{str1} kommer før {str2}.')
else:
    print(f'{str2} kommer før {str1}.')