#!/usr/bin/env python
# -*- coding: utf-8 -*-​
# REGNER UT KARAKTER UT FRA NTNUs PROSENTPOENGMETODE
# (dette eksemplet med vilje laget feil)
# INPUT: poeng
# OUTPUT: karakter
#
poeng = int(input('Skriv inn poengsummen: '))
if (poeng >= 89):
    karakter = 'A'
if (poeng >= 77):
    karakter = 'B'
if (poeng >= 65):
    karakter = 'C'
if (poeng >= 53):
    karakter = 'D'
if (poeng >= 41):
    karakter = 'E'
else:	
    karakter = 'F'
print('Karakteren ble', karakter)
