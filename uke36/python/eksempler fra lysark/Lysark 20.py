alder = int(input('Hvor gammel er du: '))
if alder < 0:
    print('Ugyldig inngangsdata.')
else:
    if alder < 18:
        print('Kom tilbake til neste valg.')
    else:                                     # Legg merke til innrykk!
        print('Fortsett til valglokalet.')
