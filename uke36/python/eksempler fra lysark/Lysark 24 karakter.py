# REGNER UT KARAKTER UT FRA NTNUs PROSENTPOENGMETODE
# INPUT: poeng
# OUTPUT: karakter
#
poeng = int(input('Skriv inn poengsummen: '))
if poeng >= 89:
    karakter = 'A'
elif poeng >= 77:
    karakter = 'B'
elif poeng >= 65:
    karakter = 'C'
elif poeng >= 53:
    karakter = 'D'
elif poeng >= 41:
    karakter = 'E'
else:   
    karakter = 'F'
print(f'Karakteren ble {karakter}')
