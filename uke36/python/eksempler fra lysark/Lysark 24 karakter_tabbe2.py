#!/usr/bin/env python
# -*- coding: utf-8 -*-​
# REGNER UT KARAKTER UT FRA NTNUs PROSENTPOENGMETODE
# (dette eksemplet med vilje laget feil)
# INPUT: poeng
# OUTPUT: karakter
#
poeng = int(input('Skriv inn poengsummen: '))
if (poeng <= 100):
    karakter = 'A'
elif (poeng <= 88):
    karakter = 'B'
elif (poeng <= 76):
    karakter = 'C'
elif (poeng <= 64):
    karakter = 'D'
elif (poeng <= 52):
    karakter = 'E'
else:	
    karakter = 'F'
print('Karakteren ble', karakter)
