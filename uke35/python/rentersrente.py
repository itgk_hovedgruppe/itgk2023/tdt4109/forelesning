'''
- Hente inn tre verdier:
    - hva står på kontoen?
    - hva er renta?
    - hvor mange år?
- Var det ikke noe med at innlesing
  gir strenger?
- Beregne hvor mye som står på kontoen
  etter så mange år:
  varpenger * rente opphøyd i antallår
  der rente er på formen 1.05 for 5%
- Skriv ut noe slikt:
  Etter 5 år har du 12.54 kroner på kontoen.
  
# Utvidet:
- Gjør dette til en funksjon, beregn_rente, og kall den etterpå.
- Er det bedre å la funksjonen ta inn tre tall,
  og heller lese inn og konvertere det på utsiden? Prøv det! 
- Kan du lage en ny funksjon start_beregning som ber brukeren
  skrive inn disse tre tallene, konvertere, og så kaller den
  den neste funksjonen beregn_rente (som altså tar inn disse
  tre variablene og returnerer verdien av funksjonen?
'''

