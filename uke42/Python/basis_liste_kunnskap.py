liste = [['Norge', 'Oslo'], ['Sverige', 'Stockholm'], ['Spania', 'Madrid'],
 ['USA', 'Washington, D.C.'], ['Finland', 'Helsinki'],
 ['Island', 'Reykjavik'], ['Danmark', 'København'],
 ['Tyskland', 'Berlin'], ['Frankrike', 'Paris']]

# Ny 'datatype' 'file'
# Som dere kanskje gradvis har forstått så finnes det noen
# grunnleggende datatyper (boolean, float...) og så er det
# noen som... lages, på et vis? (skjønt det er jo alle!)

f = open("basis_liste_kunnskap.py", 'r')
#print(f.readlines())
print(liste)


# serialisering av python-objekter. Bruk pickle!
import pickle
ut_fil = open("liste.pickle","wb") # merk b
pickle.dump(liste,ut_fil)
ut_fil.close()