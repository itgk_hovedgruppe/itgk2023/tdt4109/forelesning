'''
Forenklet
- kun 2d-liste
- enklere meny
Mål for uken:
- legge til en dictionary-variant!
- Hva er keys, values, items, unikhet... støff
'''
meny = '''alternativ:
l2\t- lag nytt land - 2d
p2\t- skriv ut info i 2d
jkh\t- jeg kan!
q - avslutt'''

liste = [['Norge', 'Oslo'], ['Sverige', 'Stockholm'], ['Spania', 'Madrid'],
 ['USA', 'Washington, D.C.'], ['Finland', 'Helsinki'],
 ['Island', 'Reykjavik'], ['Danmark', 'København'],
 ['Tyskland', 'Berlin'], ['Frankrike', 'Paris']]

def gjett_land_2d(liste):
    valg = random.choice(liste)
    print("hovedstaden i",valg[0])
    gjettet = input()
    if gjettet == valg[1]:
        print("riktig")
    else:
        print("duh")

def lag_nytt_land_2d(liste):
    nytt_land = input("nytt land: ")
    ny_h = input("ny hovedstad: ")
    liste.append([nytt_land, ny_h])
    return liste

def print_liste_2d(liste):
    for l, h in liste:
        print(f"Hovedstaden i {l} er {h}.")
    print("\n")    

def grensesnitt(liste):    
    valg = True
    while valg != 'q':
        print(meny)
        valg = (input("Valg: "))
        if (valg == "g2"): gjett_land_2d(liste)
        if (valg == "l2"): land = gjett_land_2d(liste)
        if (valg == "p2"): land = print_liste_2d(liste)
        if valg == "jkh": jeg_kan_hovedstad(liste)
        if valg == "2dd": dict = to_d_til_dict(liste)
        if valg == "pd": printd(dict) 

# Be om navn på land, finn hovedstaden
def jeg_kan_hovedstad(liste):
    funnet = False
    land = input("land: ")
    for elem in liste:
        if elem[0] == land:
            print(elem[1])
        

def gjett_dict(dict):
    

def nytt_land_dict(dict):
    

def to_d_til_dict(liste):
    dict = {}
    for kombo in liste:
        dict[kombo[0]] = kombo[1]
    return dict

def printd(dict):
    for key in dict.keys():
        print(key, dict[key])

grensesnitt(liste)

# Diskuter: Hvordan skal man gjøre om en slik todimensjonal
# liste som 'liste' til en dictionary? (funksjonen to_d_til_dict)

# Diskuter: Hvordan skal implementasjonen av gjett_dict se ut?

# Diskuter: Hvordan skal implementasjonen av nytt_land_dict se ut?

# dict.get
# Så over til å se litt mer på hva 