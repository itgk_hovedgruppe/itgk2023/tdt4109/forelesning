a = {1,2,3,4,5}
b = {3,4,5,True,'ost'}
c = a.union(b)
print(f'c: {c}')

# Hvor ble True av?
# Svar:
# Internt representeres True med verdien 1, og 1 finnes allerede i
# c. Husk at mengder ikke kan ha duplikater