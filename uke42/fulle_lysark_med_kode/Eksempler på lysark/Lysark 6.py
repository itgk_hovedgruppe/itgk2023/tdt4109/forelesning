tekst = 'En tekst som inneholder mange ord. En tekst som inneholder mange unike ord.'
tekst += ' Hvor mange unike ord er det her tro?'
tekst = tekst.replace('.','').lower()  # Fjerner . og gjør alle bokstaver små
tekst = tekst.replace('?','')          # Fjerner ?
liste = tekst.split()                  # Lager liste som inneholder alle ord i teksten
mengde = set(liste)                    # Gjør listen om til en mengde. Duplikater fjernes
print(len(liste),liste)                # Skriver lengden av listen og innholdet i den
print(len(mengde),mengde)              # Skriver lengden av mengden og innholdet i den