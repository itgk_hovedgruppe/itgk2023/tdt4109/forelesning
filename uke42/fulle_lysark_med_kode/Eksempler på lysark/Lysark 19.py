A = {3,9,12,16,22,27,30}
B = {2,7,12,16,23,31,33}

C = A.symmetric_difference(B)
print(f'Følgende tall er med i A eller B, men ikke i begge: {C}')
# kan også skrives C = A^B
C = A^B
print(f'Følgende tall er med i A eller B, men ikke i begge: {C}')