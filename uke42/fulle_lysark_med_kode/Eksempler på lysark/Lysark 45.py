# Finn gjennomsnittssummen for en av personene i tabellen

def finn_person(kar):
    print('Hvem vil du regne ut snittet for: ')
    navn = []
    for element in kar:
        navn.append(element[0])
    print(f'Følgende personer er registrert: {" ".join(navn)}')
    person = ''
    while person not in navn:
        person = input(f'Skriv inn et av navnene ({" ".join(navn)}): ')
        if person not in navn:print('\nUkjent person. Prøv igjen.\n')
    return person


kar = [['Per',87,95,23,0],['Kari',76,88,93,77],['Eva',99,88,92,83],['Martin',76,95,45,88]]
person = finn_person(kar)
funnet,i = False,0
while not funnet:
    if kar[i][0] == person:
        funnet = True
        indeks = i
    i += 1
print(f'Gjennomsnittssummen for {person} er {sum(kar[indeks][1:])/len(kar[indeks][1:])}')
