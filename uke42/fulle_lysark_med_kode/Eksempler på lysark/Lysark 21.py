A = set(input('Skriv inn mengden A (ord skilt med åpenrom): ').split())
B = set(input('Skriv inn mengden B (ord skilt med åpenrom): ').split())

print(f'\nSnittet av A og B er {A.intersection(B)}')
print(f'\nUnionen av A og B er {A.union(B)}')
print(f'\nElementer som er med i A og ikke i B: {A.difference(B)}')
print(f'\nElementer som er med i A og i B, men ikke i begge: {A^B}')
print(f'\nEr A en delmengde av B?\t {A.issubset(B)}')