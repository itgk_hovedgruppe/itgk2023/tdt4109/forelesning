# Opprette og oppdatere en dictionary

minDikt = {'Per':92925492}         # Opprette
print(minDikt)
input()

minDikt['Nils'] = 92939495         # Legge til
print(minDikt)
input()

minDikt['Nils'] = 93939596         # Endre
print(minDikt)
input()

for person in minDikt.keys():
    print(minDikt[person])
    
for navn,nummer in minDikt.items():
    print(f'{navn} har telefonnummer {nummer}')                    
    
del minDikt['Nils']               # Fjerne
print(minDikt)