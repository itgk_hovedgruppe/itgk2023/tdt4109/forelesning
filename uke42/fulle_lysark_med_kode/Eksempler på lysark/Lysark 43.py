print('Skriv inn setninger og avslutt med slutt: ')
freq = {}
line = input()
while(line != 'slutt'):
    words = line.split()
    for word in words:
        freq[word] = freq.get(word,0) + 1
    line = input()
    
for word in freq:
    print(f'{word.ljust(10)} er skrevet {freq[word]} ganger')
