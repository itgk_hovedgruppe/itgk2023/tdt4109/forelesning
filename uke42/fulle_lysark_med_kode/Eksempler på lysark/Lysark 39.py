d = {} # Oppretter et dictionary

print('Skriv inn key og verdi. Avslutt med tom streng.')

key = input('Key: ')

while(key!=''):
    value = input('Value: ')
    d[key] = value

    key = input('Key: ')

print(f'len(d): {len(d)}')
print(f'd.items(): {d.items()}')
print(f'd.keys(): {d.keys()}')
print(f'd.values(): {d.values()}')
print(f'd.get("ost",-999): {d.get("ost",-999)}')
