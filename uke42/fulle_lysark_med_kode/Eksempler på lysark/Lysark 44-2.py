kar = {'Per':[87,95,23,0],'Kari':[76,88,93,77],'Eva':[99,88,92,83],'Martin':[76,95,45,88]}
person = input('Oppgi navn: ')      # finn_person(kar)
if person in kar:
    print(f'Gjennomsnittssummen for {person} er {sum(kar[person])/len(kar[person])}')
else:
    print('Personen er ikke registrert.')