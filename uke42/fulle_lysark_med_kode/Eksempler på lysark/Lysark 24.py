import random as rn
def trekk_tall(antall):
    tall = [i for i in range(1,35)]
    vTall = []
    for i in range(antall):
        indeks = rn.randint(1,len(tall)-1)
        vTall.append(tall.pop(indeks))
    return vTall

def sjekk_tall(tall,vTall):
    vinner = tall.intersection(set(vTall[:7]))
    tillegg = tall.intersection(set(vTall[7:]))
    return len(vinner),len(tillegg)

vinnerTall = trekk_tall(10)
mineTall = trekk_tall(7)
mineV,mineT = sjekk_tall(set(mineTall),vinnerTall)
print(f'Du hadde {mineV} vinnertall og {mineT} tilleggstall.')