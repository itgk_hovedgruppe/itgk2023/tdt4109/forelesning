# Oppgave:
# En vennegjeng har blitt "toppfrelst" og skal besøke fjelltopper i Norge.
# Lag et program som finner ut hvor mange som har vært på minst en fjelltopp
# og i tillegg finner ut hvem av vennegjengen som har vært på samtlige fjelltopper

def alle_deltagere(lister):
    # INPUT: en liste av lister, hvor hver indre liste inneholder navn
    #        på deltagere som har besøkt et visst punkt av interesse
    # PROS.: gjør om de indre listene til mengder og tar snittet av dem
    #        for å finne mengden av deltagere som har vært på minst ett sted
    # OUTPUT: returnerer mengden av deltagere som har vært på minst ett sted
    # ???
    return # ?

def ivrigste_deltagere(lister):
    # INPUT: en liste av lister, hvor hver indre liste inneholder navn
    #        på deltagere som har besøkt et visst punkt av interesse
    # PROS.: gjør om de indre listene til mengder og tar snittet av dem
    #        for å finne mengden av deltagere som har vært på alle stedene
    # OUTPUT: returnerer mengden av deltagere som har vært på alle stedene
    # ???
    return # ?

fjell_lister =  [['Jo', 'Ine', 'Eli', 'Bo', 'Ron', 'Sam', 'Una', 'Ron'],
                 ['Eli', 'Ada', 'Oda', 'Jo', 'Dag', 'Una', 'Ron'],
                 ['Bo', 'Ada', 'Tor', 'Dag', 'Jo', 'Eli', 'Frank']]
print(f'Alle deltagere (minst ett fjell): {alle_deltagere(fjell_lister)}')
print(f'Vært på alle {len(fjell_lister)} fjell: {ivrigste_deltagere(fjell_lister)}')
