# Finn gjennomsnittssummen for en av personene i tabellen

def finn_person(kar):
    print('Hvem vil du regne ut snittet for: ')
    personliste = list(kar.keys())
    print(f'Følgende personer er registrert: {" ".join(personliste)}')
    person = ''
    while person not in kar.keys():
        person = input(f'Skriv inn et av navnene ({" ".join(personliste)}): ')
        if person not in kar.keys():print('\nUkjent person. Prøv igjen.\n')
    return person


kar = {'Per':[87,95,23,0],'Kari':[76,88,93,77],'Eva':[99,88,92,83],'Martin':[76,95,45,88]}
person = input('Oppgi navn: ')      # finn_person(kar)
print(f'Gjennomsnittssummen for {person} er {sum(kar[person])/len(kar[person])}')