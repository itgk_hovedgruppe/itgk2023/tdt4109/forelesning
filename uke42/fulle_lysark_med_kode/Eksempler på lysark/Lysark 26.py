print('På hvert spørsmål skal du skrive inn noen navn')
print('med mellomrom mellom, f.eks. Ole Nina Per Anne')
print('Det bør være noe overlapp mellom listene.')

streng_V = input('Hvem kan ha hatt tilgang til våpenet? ')
V = set(streng_V.split())

streng_M = input('Hvem hadde motiv for forbrytelsen? ')
M = set(streng_M.split())

streng_A = input('Hvem har alibi? ')
A = set(streng_A.split())

mistenkte = V.intersection(M).difference(A)

print(f'Mulige mistenkte kan være {mistenkte}')