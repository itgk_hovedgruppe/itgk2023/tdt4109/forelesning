d ={'navn':'Ole','alder':42,'weight':45} #Oppretter dict

print(d)
print(list(d.keys()))
input()
print('\nKeys non-sorted:\n')
for x in d:
    print(f'{x}\t{d[x]}')

print('\nKeys sorted:\n')
for x in sorted(d.keys()): # sorterer keys
    print(f'{x}\t{d[x]}')
