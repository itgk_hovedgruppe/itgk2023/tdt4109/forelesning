#En mengde kan lages ved å tilordne en serie elementer inne i {}
D = {9,4,1,3} # Lager en mengde

#En kan konvertere lister og strenger ved å bruke funksjonen set(x)
A = set([5,3,3,12])   # NB: Mister duplikater!
B = set([True,'ost',23.2,92,False])
C = set('aaabcd') # gir mengden {'a','b','c','d'}
E = set()       # gir en tom mengde
print(f'A: {A}\nB: {B}\nC: {C}\nD: {D}\nE: {E}')

#Legge til og fjerne elementer fra en mengde:
A.add(9)        # Legg til ett element. in-place
A.update([3,4,2]) # Legg til flere elementer in-place
B.remove(92)      # fjern et element  in-place
print(f'A: {A}\nB: {B}')