# Et program som leser inn et tall fra tastaturet
# og konverterer det til float
feil = True
while feil:
    try:
        tall = input('Oppgi et tall: ')
        print(float(tall))
        feil = False
    except:
        print('Noe gikk galt. Skrev du et tall? Du må skrive', end = ' ')
        print('3, ikke Tre.')
