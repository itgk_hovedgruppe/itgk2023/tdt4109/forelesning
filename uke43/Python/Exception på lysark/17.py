def leggTilNoe(dikt):
    try:
        navn=input('For hvem vil du oppdatere dictionarien: ')
        leggTil = input('Oppgi det som skal legges til: ')
        dikt[navn].append(leggTil)
    except AttributeError:
        dikt[navn] = [dikt[navn],leggTil]
    except KeyError:
        dikt[navn] = leggTil

minDikt = {'Per':92925492}      # Opprette
print(minDikt)

minDikt['Nils'] = 92939495      # Legge til
print(minDikt)

leggTilNoe(minDikt)

print(minDikt)