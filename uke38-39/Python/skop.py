

def legg_ti_til_tall(tall):
    # tall er også en lokal variabel, men navnet overskygger
    # tall fra hovedprogrammet.
    tall += 10 # Begge heter tall! 
    print(f'Hvordan vet denne funksjonen at navn er "{navn}"?')
    lokal_variabel =  'hemmelig'
    return tall


navn = 'Børge'
tall = 10 # Begge heter tall!
nyttall = legg_ti_til_tall(tall)
print(tall, nyttall) # Se hvordan tall her er 10
print(lokal_variabel) # Hvorfor feiler denne?

