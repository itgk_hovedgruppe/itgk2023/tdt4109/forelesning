import math # Importerer alt fra matte, med alias
from math import exp as ekks # Importerer bare funksjonen exp med alias.


# # Vi har alle ting her. Som cosinus
ganger = 0
while ganger < 10:
    for i in range(0,360,10):
        rads = (2*math.pi)*(i/360)

        my_cos = 10 + int(math.cos(rads)*10)
        print("#"*my_cos)
    ganger += 1
    
# Det samme kan en si om tangens, logaritmer, alt slikt.
liste = []
for x in range(10):
    print(x, ekks(x)) # e^x
    break # Husker du hva denne gjør?
    print(x, matte.e**x) # e^x
    
