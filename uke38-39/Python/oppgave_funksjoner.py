# Del 1:
# Lag en funksjon differanse(tall1, tall2)
# Den skal _returnere_ differansen mellom tallene,
# absoluttverdien
# Inntil videre kaller vi bare denne ved at du skriver ut
# resultatet av et kall til differanse() med to valgfrie tall

def dinneranse(tall1, tall2):
    return tall2-tall1

assert dinneranse(10,1) == 9, "10 og 1 skulle vært ni"
assert dinneranse(1,10) == 9, "1 og 10 skulle vært ni"
# Del 2:
# Lag en funksjon tilfeldig_tall som spør brukeren om
# en maksgrense (heltall). Funksjonen skal returnere et tilfeldig tall
# mellom 0 og til og med dette tallet.
# Husk import random og random.randint()
# Se for eksempel https://www.geeksforgeeks.org/python-randint-function/

# Del 3
# Lag funksjonen main()
# Den skal hente inn to tilfeldig_tall.
# Så skal den skrive ut tallene og absoluttverdien av differansen.
# Bruk metodene du ha laget før!
# kjør så main()