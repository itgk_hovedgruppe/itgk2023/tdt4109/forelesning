'''
I Python 3.6 og senere kan vi spesifisere datatypen
til parametre og retur. Dette kalles 'type hints' og
kan være veldig nyttig til å sikre at man jobber på riktig
måte. I språk som Java MÅ en gjøre dette, så de som mekker
Python har senere funnet ut at litt rigiditet faktisk kan
være en god ting.
I Thonny vil en kunne se resultatet av type hints hvis en
åpner assistenten (view-menyen). Etter kjøring vil man
kunne lese hva tolkeren mener er galt.
Hvis man kjører python fra kommandolinjen må man installere
en hjelpemodul, feks mypy (pip install mypy) og så kjøre
mypy spesifisert.py
'''
'''
Skal legge sammen strengen og tallet, og returnere resultatet.
'''
def spefisiert(streng:str, tall:int) -> str:
    return streng+str(tall)

print(spefisiert("abcde","4"))