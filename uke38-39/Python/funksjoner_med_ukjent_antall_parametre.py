'''
Hvis du ønsker å lage en funksjon som kan ta inn et ukjent antall
parametre, da kan du bruke * før et variabelnavn. (søk opp 'python *args') 
Man kan spesifisere faste variable også, ved å legge dem først.

Det er standard å kalle parameteren for *args, men den kan hete 
hva som helst.
'''

def funksjoner_med_ukjent_antall_parametre(var1, *args):
    print(f'Dette er args: {args}.')
    print(f"Dette er var1: {var1}")
    print(f'Dette er parametrene etterpå: {", ".join(args)}\n')
    
    
funksjoner_med_ukjent_antall_parametre('a')
funksjoner_med_ukjent_antall_parametre('a', 'b')
funksjoner_med_ukjent_antall_parametre('a', 'b', 'c')

