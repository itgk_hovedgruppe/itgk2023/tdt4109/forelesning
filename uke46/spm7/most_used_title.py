'''
Mest brukte tittel, hmm...
Ok, da må vi telle antallet år en tittel er i bruk. Så, en dictionary med
tittel som nøkkel, og årstall legges inn i liste i verdi. Så kan vi sammenlikne
størrelsen på dem etterpå.
'''

def parse_movie(line):
    split_line = line.split(",")
    if len(split_line) == 4: # Hvis ikke komma i tittel
        return split_line[:-1] # Gidder ikke ta med distribusjonsdato
    # Den neste linjen er litt festlig...
    # Vi setter sammen alle andre enn de siste 3 elementene i splitten
    # Deretter fjerner vi første og siste tegn, siden de var '"'
    title = "".join(split_line[:-3])[1:-1]
    return [title,split_line[-3],split_line[-2]]


def read_movies():
    with open("film_utgivelser.csv") as f:
        f.readline()
        lines = f.readlines()
    movies = []
    for line in lines:
#        movies.append(line.split(","))
        movies.append(parse_movie(line))
    return movies

def movies_in_country(movies,country):
    how_many = []
    for movie in movies:
        if movie[2] == country:
            how_many.append(movie)
    return how_many
    
def num_distributions_for_year(movies, year):
    how_many = []
    for movie in movies:
        if movie[1] == year:
            how_many.append(movie)
    return how_many

def distributions_per_title(movies):
    distros = {}
    for title, year, country in movies:
        distros[title] = distros.get(title, 0)+1
    return distros

def num_title_used(movies):
    distros = {}
    for title, year, country in movies:
        prev_years = distros.get(title, [])
        if year not in prev_years: # Hva skjer uten denne sjekken?
            distros[title] = prev_years+[year]
    return distros

def print_most_used_title(movies):
    # Mekke dict med tittel som nøkkel, og så liste med år som verdi
    titles_used = num_title_used(movies)

    # Finne den lengste listen av år for distribusjon
    max_years = 0
    for title, distro_years in titles_used.items():
        if len(distro_years) > max_years:
            max_years = len(distro_years)
    
    print("Mekka dict...")
    # Så lage en liste over de filmene som er brukt flest ganger. Det kan jo være flere!
    for title, distro_years in titles_used.items():
        if len(distro_years) == max_years:
            print(f"{title} er laget flest ganger ({len(distro_years)}), i årene {', '.join(distro_years)}.")

movies = read_movies()
print_most_used_title(movies)

# Men hva hvis vi vil kunne filtrere flest filmer fra bare USA?
print_most_used_title(movies_in_country(read_movies(),"USA"))

# Kanskje most_used_title også burde returnert en liste av et slag, som så printes,
# men det lar jeg være en oppgave til leseren. Men, generalisering er generelt bra!
# Videre, burde den første dict-mekkingen i pmut vært sin egen funksjon? Da kunne jeg
# listet opp de fem mest brukte titlene i et gitt land og slikt. Kult!


