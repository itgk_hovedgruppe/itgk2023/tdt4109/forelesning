def parse_movie(line):
    split_line = line.split(",")
    if len(split_line) == 4: # Hvis ikke komma i tittel
        return split_line[:-1] # Gidder ikke ta med distribusjonsdato
    # Den neste linjen er litt festlig...
    # Vi setter sammen alle andre enn de siste 3 elementene i splitten
    # Deretter fjerner vi første og siste tegn, siden de var '"'
    title = "".join(split_line[:-3])[1:-1]
    return [title,split_line[-3],split_line[-2]]

def read_movies():
    with open("../film_utgivelser.csv") as f:
        f.readline()
        lines = f.readlines()
    movies = []
    for line in lines:
#        movies.append(line.split(","))
        movies.append(parse_movie(line))
    return movies

def movies_in_japan(movies):
    how_many = 0
    for movie in movies:
        if movie[2] == "Japan":
            how_many += 1
    print(f"Det er distribuert {how_many} filmer i Japan.")
    

movies = read_movies()
movies_in_japan(movies)