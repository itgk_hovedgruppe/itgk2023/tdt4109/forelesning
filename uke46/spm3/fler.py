def parse_movie(line):
    split_line = line.split(",")
    if len(split_line) == 4: # Hvis ikke komma i tittel
        return split_line[:-1] # Gidder ikke ta med distribusjonsdato
    # Den neste linjen er litt festlig...
    # Vi setter sammen alle andre enn de siste 3 elementene i splitten
    # Deretter fjerner vi første og siste tegn, siden de var '"'
    title = "".join(split_line[:-3])[1:-1]
    return [title,split_line[-3],split_line[-2]]

def read_movies():
    with open("../film_utgivelser.csv") as f:
        f.readline()
        lines = f.readlines()
    movies = []
    for line in lines:
#        movies.append(line.split(","))
        movies.append(parse_movie(line))
    return movies

def movies_in_country(movies,country):
    antall = 0
    for movie in movies:
        if movie[2] == country:
            antall += 1
    return antall
    

movies = read_movies()
japan = movies_in_country(movies, "Japan")
norway = movies_in_country(movies, "Norway")

print(f"Det er distribuert {japan-norway} flere filmer i Japan enn i Norge.")