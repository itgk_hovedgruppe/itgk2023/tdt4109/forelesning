
def read_movies():
    with open("../film_utgivelser.csv") as f:
        f.readline()
        lines = f.readlines()
    movies = []
    for line in lines:
       movies.append(line.split(","))
    return movies

def movies_in_japan(movies):
    how_many = 0
    for movie in movies:
        if movie[2] == "Japan":
            how_many += 1
    print(f"Det er distribuert {how_many} filmer i Japan.")
    

movies = read_movies()
movies_in_japan(movies)