'''
Nå skal i finne hvilken tittel som har flest utgivelser.
Nå kan vi begynne å tenke strukturert med en gang. La oss
opprette en dict med antall utgivelser som nøkkel, og en liste
med land som verdi. Så kan vi ta maks av den.
'''

def parse_movie(line):
    split_line = line.split(",")
    if len(split_line) == 4: # Hvis ikke komma i tittel
        return split_line[:-1] # Gidder ikke ta med distribusjonsdato
    # Den neste linjen er litt festlig...
    # Vi setter sammen alle andre enn de siste 3 elementene i splitten
    # Deretter fjerner vi første og siste tegn, siden de var '"'
    title = "".join(split_line[:-3])[1:-1]
    return [title,split_line[-3],split_line[-2]]


def read_movies():
    with open("../film_utgivelser.csv") as f:
        f.readline()
        lines = f.readlines()
    movies = []
    for line in lines:
#        movies.append(line.split(","))
        movies.append(parse_movie(line))
    return movies

def movies_in_country(movies,country):
    how_many = []
    for movie in movies:
        if movie[2] == country:
            how_many.append(movie)
    return how_many
    
def num_distributions_for_year(movies, year):
    how_many = []
    for movie in movies:
        if movie[1] == year:
            how_many.append(movie)
    return how_many

# Ny funksjon som hjelper oss på veien mot svaret på flest distribusjoner
def distributions_per_title(movies):
    distros = {}
    for title, year, country in movies:
        distros[title] = distros.get(title, 0)+1
    return distros

movies = read_movies()
distros = distributions_per_title(movies)
max_distros = max(distros.values())
for title, num_distros in distros.items():
    if num_distros == max_distros:
        print(f"{title} har flest distribusjoner, med {num_distros}.")