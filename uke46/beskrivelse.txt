Dagens oppgave - filminformasjon!

Vi har en 'csv-fil' kalt 'filmer.csv'. En typisk måte å representere data, og kan importeres i Excel og slikt.
Vi ønsker følgende svar fra disse dataene: Hvert spørsmål kan påvirke funksjoner og oppførsel
- 1: Hvor mange filmer er gitt ut i Japan? 
- 2: Noen filmer har komma i tittelen - hva gjør vi med det?
- 3: Hvor mange fler filmer er distribuert i "Japan" enn i "Norway"?
- 4: Hvor mange distribusjoner har alle filmer laget i 1973? 
- 5: Hvor mange filmer ble gitt i "Norway" i 1963?
- 6: Hvilken filmtittel har flest utgivelser?
- 7: Hvilke år ble mest brukte tittel laget? Du kan forutsette at det kun er én film med samme tittel per år. Det KAN være flere titler som er brukt flest ganger, ta høyde for det.

Hvor mange filmer ble gitt ut i Norge det året det ble gitt ut flest filmer i hele verden, og hvilket år var det.

# Filinformasjon
Hver linje er en ny film, med følgende struktur:
- Tittel, produksjonsår, land, distribusjons_dato (vi spør aldri om den siste)
- Første linje er en overskrift, den kan vi ikke ha med.
- En tittel kan være distribuert til mange land!
- Samme tittel kan brukes på flere filmer, men disse vil ha så fall ha ulike produksjonsår.

