'''
Det viktige i denne delen er at en ser at funksjonene som teller antall filmer
per år eller land ikke bør returnere antallet, men at en heller bør
fungere som et filter. En liste inn gir en ny, filtrert liste ut!
'''

def parse_movie(line):
    split_line = line.split(",")
    if len(split_line) == 4: # Hvis ikke komma i tittel
        return split_line[:-1] # Gidder ikke ta med distribusjonsdato
    # Den neste linjen er litt festlig...
    # Vi setter sammen alle andre enn de siste 3 elementene i splitten
    # Deretter fjerner vi første og siste tegn, siden de var '"'
    title = "".join(split_line[:-3])[1:-1]
    return [title,split_line[-3],split_line[-2]]


def read_movies():
    with open("../film_utgivelser.csv") as f:
        f.readline()
        lines = f.readlines()
    movies = []
    for line in lines:
#        movies.append(line.split(","))
        movies.append(parse_movie(line))
    return movies

# Nå må vi skrive om alt, så funksjonene returnerer lister med innhold og ikke antall.
# Vi kan se på funksjonene movies_in_country og num_distributions_for_year mer som filter.

def movies_in_country(movies,country):
    how_many = []
    for movie in movies:
        if movie[2] == country:
            how_many.append(movie)
    return how_many
    
def num_distributions_for_year(movies, year):
    how_many = []
    for movie in movies:
        if movie[1] == year:
            how_many.append(movie)
    return how_many

movies = read_movies()
norway = movies_in_country(movies, "Norway")
norway_1963 = num_distributions_for_year(norway, "2006")
print(f"Det er distribuert {len(norway_1963)} filmer laget i 1963 i Norge.")
print(f"Det er distribuert {len(num_distributions_for_year(movies_in_country(movies,'USA'),'2002'))} filmer laget i 2002 i USA.")
print(num_distributions_for_year(movies_in_country(movies,'USA'),'2002'))