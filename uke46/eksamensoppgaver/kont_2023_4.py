def respons(fil, r, c):
    try:
        with open(fil) as f:
            line = f.readlines()[r].split(";")
            if c == 0:
                print(line[3].strip(), int(line[c]))
                return line[3].strip()[int(line[c])]
            else:
                return float(line[c])
    except FileNotFoundError:
        return -1
    except Exception as e:
        return e
    
print(respons("datafil.txt",3,0))