def parse_movie(line):
    movie_line = line.split(",")
    if len(movie_line) == 4:
        return movie_line[:-1]
    title = "".join(movie_line[:-3])
    return [title, movie_line[-3], movie_line[-2]]

def read_file():
    data = []
    with open("film_utgivelser.csv") as f:
        f.readline()
        for line in f.readlines():
            data.append(parse_movie(line))
    return data
            
            
def dist_in_country(movies, country):
    how_many = []
    for dist in movies:
        if dist[2] == country:
            how_many.append(dist)
    return how_many            

def dist_in_year(movies, year):
    how_many = []
    for dist in movies:
        if dist[1] == year:
            how_many.append(dist)
    return how_many            



movies = read_file()
japan = dist_in_country(movies, "Japan")
norway = dist_in_country(movies, "Norway")
print(f"Det er {len(japan)-len(norway)} manko for Norge.")
print(f"Det er {len(dist_in_year(movies, '1973'))} filmer laget i 1973.")

norge1963 = dist_in_year(norway,"1963")
