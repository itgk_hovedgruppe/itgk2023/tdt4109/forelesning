liste = ['Hans','Grete','Askpepott','Stoltenberg']

for navn in liste:
    print(navn)

# Denne bruker plasseringsrekkefølgen, med en peker til hver plass.
# En typisk feil er å gjøre en 'off-by-one' her. Forsøk å sette den til range(5)!
for i in range(4): #  Fra 0 til og UTEN
    print(f"På plass nummer {i} kommer...")
    print(liste[i])
    
# LURT: Bruk den første. Hvis du trenger plassering, bruk enumerate
for plass, navn in enumerate(liste):
    print(f"På plass nummer {plass} kommer {liste[plass]}.")
