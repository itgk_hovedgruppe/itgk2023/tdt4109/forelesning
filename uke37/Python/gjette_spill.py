import random
number = random.randint(0,101)

guess = -1
while guess != number:
    guess = int(input("Guess the number: "))
    if guess < number:
        print('Too low!')
    elif guess > number:
        print("Too high!")
    else:
        print("Spot on!")
