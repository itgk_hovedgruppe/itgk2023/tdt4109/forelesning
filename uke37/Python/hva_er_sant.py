if 0:
    print('0 er True')
else: print('0 er False')

if 1:
    print('1 er True')
else: print('1 er False')

if []:
    print('[] er True')
else: print('[] er False')

if [""]:
    print('[""] er True')
else: print('[""] er False')

if "":
    print('"" er True')
else: print('"" er False')

if "a":
    print('"a" er True')
else: print('"a" er False')