'''
# Oppgave:
# Be brukeren om å skrive inn nye tall helt til tallet er 0
# Da skal du skrive ut summen av tallene
'''
# sum = 0
# tall = int(input("Skriv inn tall, '0' avslutter: "))
# while tall !=0:
#     sum += tall
#     tall = int(input("Skriv inn tall, '0' avslutter: "))
# print(sum)
# # eller enda bedre
# sum = 0
# tall = -1
# while tall:
#     tall = int(input('tall: '))
#     sum += tall
# print(sum)


''' 
# Oppgave!
Skriv ut alle kvadrattallene til tallene i tregangen til 100 på formatet:
'''
# for i in range(0,100,3):
#     print(f"Kvadratet til {i} er {i**2}." )


''' 
# Oppgave!
# Be brukeren om et tall 'n'
# Skriv ut n-gangen mellom 0 og 100
# For de som blir fort ferdige: fjern at den skriver ut 0.
'''
# n = int(input("n: "))
# for i in range(n,100,n):
#     print(i)



# Skriv ut annethvert element i listen gitt under:
liste = [chr(x) for x in range(33,126)]
# Måten å skrive ting over er IKKE pensum, bare godta! ;)
# Bare en måte jeg kan bruke for å raskt lage en liste med innhold.
# For de som uansett vil lære seg det kalles det 'list comprehension'
# Den gjør det samme som
liste = []
for x in range(33,126):
    liste.append(chr(x))


# Ofte raskere kodemåte. Du kan bruke det på eksamen selv om != pensum. 
# for i in range(0,len(liste),2):
#     print(liste[i])