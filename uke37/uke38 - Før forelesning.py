'''
Start med kahoot:
https://create.kahoot.it/share/starting-out-with-python-3-logikk/1f6d063e-9f5e-40e1-bfd8-ec21d8f0fec9

Så en rask menti:

Et par ting jeg ikke rakk å svare på sist.
Format. Sjekk ut https://realpython.com/python-f-strings/
'''
# print(f"{2/7:.2f}")
'''
Negativt tall binært - hva om du lar biten helt til venstre bestemme fortegn? MSB
Hvordan levere øving - legg inn på bb? Lag en zipfil med alle filene dine om det trengs.
Hvor er Helle? Ikke så langt unna Kragerø. Fine kniver!
elif - hvis du har en if, og testen er False, så vil en elif: testes etterpå.
sorteringsalgoritmer - https://www.youtube.com/watch?v=BeoCbJPuvSE

Så over til dagens tema: løkker

Vi har to typer:
while: betingelseskontrollert løkke. Gjør noe så lenge testen er True.
- while kan være 'evig' så lenge testen er True. Eksempel kommer!
for: antallskontrollert løkke. Gjør noe et visst antall ganger.


Skrive ut lengden av en tekststreng
'''
# streng = input("Skriv inn streng: ")
# print(f"Lengden av '{streng}' er {len(streng)}.")

'''
Kan en søke etter et tegn i en streng?
'''
# streng = input("Skriv inn streng: ")

'''
Neste linje er en automatisk løkke som leter etter tegnet!
Returnerer True/False alt etter om den finner det.
'''
# if 'e' in streng: 
#     print(f"Tegnet 'e' er i {streng}.")

'''
Hvis jeg vil gjøre dette mange ganger på rad...
Jeg kan kjøre programmet flere ganger
Jeg kan kopiere inn det samme flere ganger
Men kan en ikke repetere en viss kode flere ganger i stedet?
'''
# while True:
#     streng = input("Skriv inn streng: ")
#     print(f"Lengden av '{streng}' er {len(streng)}.")

'''
Men dette blir jo litt kjipt, en må kræsje programmet
Hva om en kan lage løkken slik at den avsluttes hvis
brukeren taster inn en spesiell ting, som en tom streng?
Jeg vet ikke hvor mange ganger jeg skal gjøre en ting!
'''
# streng = input("Skriv inn streng (tom streng avslutter): ")
# while streng != "":
#     print(f"Lengden av '{streng}' er {len(streng)}.")
#     streng = input("Skriv inn streng (tom streng avslutter): ")

'''
Hva om en har behov for å kjøre en løkke et visst antall ganger?
Eksempel: Kan jeg skrive ut alle bokstavene i en streng,
bokstav for bokstav?
Jeg kan bestemme på forhånd hvor mange ganger noe skal gjøres!
'''
# while (streng := input("Skriv inn streng tom streng avslutter: ")) != "":
#     for bokstav in streng:
#         print(f"Gi meg en {bokstav}!")
    

'''
Oppgave! ##########################################################
Be brukeren om å skrive inn nye tall helt til tallet er 0
Da skal du skrive ut summen av tallene
'''
# tall = 0
# lest = input("Skriv inn et tall, 0 avslutter: ")
# while lest != 0:
#     tall = tall + lest
#     lest = input("Skriv inn et tall, 0 avslutter: ")
# print(f"Summen er {tall}.")
    

'''
while kan også brukes på denne måten:
'''
# i = 0
# while i<10:
#     print(i)
#     i+=1 


'''
Noen ganger vil man gjerne gjenta noe, for å slippe å skrive det mange ganger.
Grafisk eksempel - Hva kan jeg skrive for å gjøre dette for et gitt antall #?
'''

# print("#")
# print("##")
# print("###")
# print("####")
# print("#####")
# print("######")
# print("#######")
# print("######")
# print("#####")
# print("####")
# print("###")
# print("##")
# print("#")

'''
Kan jeg gjøre noe... et bestemt antall ganger?
# '''
# for i in range(9):
#     print(i)

'''    
Trenger ikke starte med 0... range(start,slutt, hoppavstand)
'''
# for i in range (100,10,-2):
#     print(i)

'''
Man kan gjerne kalle en egendefinert funksjon inni en løkke.
Det kan gjøre løkka mye enklere å lese!
Hvis man gjør samme ting flere steder i koden er det lurt
å lage en funksjon for det, og så kalle samme sted!
Merk: Denne har litt mer avansert funksjonsbruk, hvis det
ikke er helt forståelig kommer vi tilbake til det senere!
'''

def beregn_masse(tall, ganger):
    # Her kan en ha en lang beregningsgreie...
    tmptall = tall
    for i in range(ganger-1):
        tmptall += tall
    return tmptall
# 
for i in range(10):
    if i%2: # hvorfor fungerer i%2? Må en ikke bruke i%2 == True?
        print(f"{i} er ikke i togangen, dobbelt opp blir {beregn_masse(i, 2)}")
    else:
        print(f"{i} er i togangen, trippelt opp blir {beregn_masse(i, 3)}")
#     


'''
Ingen problemer å kombinere while med for: (importer time!)
'''
# import time
# 
# while True:
#     for i in range(1,10):
#         time.sleep(0.1)
#         print("%"*i) # Skriv tegnet % i ganger etter hverandre
#     for i in range(9,0,-1):
#         time.sleep(0.1)
#         print("#"*i) # Skriv tegnet % i ganger etter hverandre

'''
Oppgave! ##########################################################
Skriv ut alle kvadrattallene til tallene i tregangen til 100 på formatet:
Kvadratet til 3 er 9.
Kvadratet til 6 er 36.
...
'''
# for i in range(0,100,3):
#     print(f"Kvadratet til {i} er {i**2}")

'''
Oppgave! ##########################################################
Be brukeren om et tall 'n'
Skriv ut n-gangen mellom 0 og 100
Format på utskriften skal bli
3 ganger 0 er 0.
3 ganger 1 er 3.
...
For de som blir fort ferdige: fjern at den skriver ut 0.
'''




'''
Tenk på et tall mellom 0 og 100!
Få beskjed om det er for høyt, for lavt eller perfekt

Pseudokode:
- Trekk et tilfeldig tall mellom 0 og 100
- Gjenta resten til bruker velger rett tall:
    - Be brukeren om å taste inn et tall
    - Er det for lavt, si ifra og be om nytt tall
    - Er det for høyt, si ifra og be om nytt tall
    - Er det riktig tall: gratulere, avslutte
    
Her må vi forteller tolkeren at den skal hente inn
noen flere funksjoner, fra 'random'. Det er spesielt
tilfeldig trekking av heltall vi er ute etter, randint.
'''

import random
# number = random.randint(0,101)
# print(number)
# #
# antall = 0
# guess = -1
# while guess != number:
#     guess = int(input("Guess the number: "))
#     antall += 1
#     if guess < number:
#         print('Too low!')
#     elif guess > number:
#         print("Too high!")
#     else:
#         print(f"Spot on! {antall}")

'''
Så en tekststreng er egentlig en liste av bokstaver...
Finnes det andre lister?
La meg teste....
'''
# liste = [1, 2, 3, 5, 7, 11]
# for i, tall in enumerate(liste):
#     print(f"{tall}*2 = {tall*2} {i}")

# for i in range(len(liste)):
#     print(liste[i])

''' Hver enkelt element kan finnes med [elementnummer]'''
# print(liste[1]) # Vi starter med element nummer 0!

'''
En for-løkke går altså igjennom ett og ett element
Men... bare tall?
'''
# liste = [1, 2, 3, 5, 7, 11]
# liste[2] = "femten"
# for tall in liste:
#     print(f"{tall}*2 = {tall*2}")

'''
Så en liste kan ha elementer med ulik type!
Et element i en liste kan faktisk være en egen liste,
hvordan vil i så fall det se ut?
'''

'''
Hvordan kan lister og bruk av range() sammenliknes?
'''
# for n in range(10): print(n)
# # Er det samme som
# for n in [0,1,2,3,4,5,6,7,8,9]: print(n)

'''
Hoppe to og to tall hver gang, til og uten 6.
'''
# for n in range(1,6,2): print(n)
# # Er det samme som
# for n in [1,3,5]: print(n)

'''
Gå baklengs tre hopp hver gang:
'''
# for n in range(10,1,-3): print(n)
# # Er det samme som
# for n in [10,7,4]: print(n)

'''
Man KAN bruke vold for å komme seg ut at kinkige situasjoner.
Det er ikke noe jeg liker, men break og continue finnes:
'''
# liste = [x for x in range(10)] # [0, 1, ... , 9]
# for tall in liste:
#     if tall == 3:
#         continue # gå tilbake til start
#     if tall == 6:
#         break # Avslutt hele løkkegreia
#     print(tall)
# print('Ferdig')

'''
Man bruker ofte løkker til validering:
'''
# streng = ""  
# while len(streng) < 3:
#     streng = input("Skriv inn en streng på minst 3 tegn: ")
# print(f"Du skrev inn: {streng}")

'''
Ta en kikk på løkke i løkke i løkke, lysark 29/30
'''

'''
Vi kan kontrollere flyten i løkker
break - tar oss helt ut av den løkken vi er i (den innerste)
continue - hopper opp til starten av løkken og kjører neste iterasjon
'''
# liste = [x for x in range(10)]
# for tall in liste:
#     if tall == 5:
#         print("Vi er ferdige nå")
#         break
#     if (tall%2):
#         continue
#     print(f"{tall}*2 = {tall*2}")

'''
Til slutt noe om kortformer. Hent opp lysark side 25
'''

# Oppgave! ##########################################################
# Skriv ut annethvert element i listen gitt under:
liste = [chr(x) for x in range(33,126)]
# Måten å skrive ting over er IKKE pensum, bare godta! ;)
# Bare en måte jeg kan bruke for å raskt lage en liste med innhold.
# For de som uansett vil lære seg det kalles det 'list comprehension'
# Ofte raskere kodemåte. Du kan bruke det på eksamen selv om != pensum. 