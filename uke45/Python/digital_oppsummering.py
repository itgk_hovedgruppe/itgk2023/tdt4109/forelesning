'''
I dag: oppsummere de viktigste tingene, se om vi rekker å komme inn på mer.
Men det fokus på å forklare hovedtrekk, og så se om det er noe vi kan hente
inn fra chat.
Tema:
'''

# Funksjoner: La oss gjøre alt gjennom funksjoner
# husk på noe mer returverdi og ikke.
# skal jeg vise dem at når en sender inn en liste til en funksjon,
# og så endrer den inni funksjonen, så endres den selv om en ikke
# returnerer den? Du vet, det om muterbarhet?

# def dobbeloppskiten(inn):
#     # Bare gang det med to:
#     print(inn*2)
# 
# print(dobbeloppskiten(12))



# Lister
#for ord in ('Rabarbra er egentlig ganskt godt, men det er litt spesi også.'.split()):
#     print(ord)
# vise litt splicing
#liste = 'Rabarbra er egentlig ganskt godt, men det er litt spesi også.'.split()


# Filer: ode.txt
# hvor mange ord er det per linje?
# kan vi lage en todimensjonal liste som viser linje og antall ord?
'''
with open('ode.txt', 'r', encoding='utf-8') as f:
    epos = f.readlines()
    ord_mengde = []
    for i in range(len(epos)):
        print(i, len(epos[i].split()))
        ord_mengde.append((i, len(epos[i].split())))
        


maks = -1
makslinjenr = -1

for i, linje in enumerate(ord_mengde):
    if linje[1] > maks:
        maks = linje[1]
        makslinjenr = i
print(maks, makslinjenr)
'''
    
  
    
    

# 2d-lister


# Nøstede løkker


# dictionary

# tupler (åfferea? De er den kijpe lillebroren til lister,
# den som ikke får lov til noe.)

# Rekursjon - hva skjer med muterbare datatyper?
def flikk_med_liste(liste):
    print(liste)
    if len(liste) == 1: return [liste]
    liste[0] = liste[0] + liste[-1]
    del liste[-1]
    return flikk_med_liste(liste)


liste = [0, 1, 2, 3, 4]
print(flikk_med_liste(liste))